import './Cell.css'

const Cell = ({ active, onMouseDown, onMouseUp, onMouseEnter }) => {
  return (
    <span
      className={`cell${active ? ' active' : ''}`}
      onMouseDown={onMouseDown}
      onMouseUp={onMouseUp}
      onMouseEnter={onMouseEnter}
    />
  )
}

export default Cell
