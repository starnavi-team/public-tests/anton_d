import { useState, useEffect, useMemo } from 'react'
import { cloneDeep } from 'lodash'

import Cell from './Cell'

import { generateGrid } from './utils'

import './Grid.css'

const Grid = ({ width, height }) => {
  const [grid, setGrid] = useState(generateGrid(width, height))
  const [isSelectionInProgress, setIsSelectionInProgress] = useState(false)
  const [{ startRow, startCol }, setSelectionStartCoords] = useState({})
  const [{ currentRow, currentCol }, setSelectionCurrentCoords] = useState({})
  const [{ prevRow, prevCol }, setSelectionPrevCoords] = useState({})
  const isStartCellActive = useMemo(
    () => {
      if (isSelectionInProgress) {
        return grid[startRow][startCol]
      }
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isSelectionInProgress, startCol, startRow]
  )

  useEffect(() => {
    if (isSelectionInProgress) {
      const currentSelectionDirection = {
        left: startCol > currentCol,
        right: startCol < currentCol,
        up: startRow > currentRow,
        down: startRow < currentRow,
      }

      const prevSelectionDirection = {
        left: startCol > prevCol,
        right: startCol < prevCol,
        up: startRow > prevRow,
        down: startRow < prevRow,
      }

      const deselected = {
        left: prevSelectionDirection.right && currentCol < prevCol,
        right: prevSelectionDirection.left && currentCol > prevCol,
        up: prevSelectionDirection.down && currentRow < prevRow,
        down: prevSelectionDirection.up && currentRow > prevRow,
      }

      const isSelectionJustStarted =
        startRow === currentRow && startCol === currentCol

      const isDeselected = Object.keys(deselected).some(
        (direction) => deselected[direction]
      )

      const computeNextGrid = () => {
        const nextGrid = cloneDeep(grid)

        if (isDeselected) {
          if (deselected.up || deselected.down) {
            for (
              let row = prevRow;
              deselected.up ? row > currentRow : row < currentRow;
              deselected.up ? row-- : row++
            ) {
              for (
                let col = startCol;
                prevSelectionDirection.left ? col >= prevCol : col <= prevCol;
                prevSelectionDirection.left ? col-- : col++
              ) {
                nextGrid[row][col] = isStartCellActive
              }
            }
          }

          if (deselected.left || deselected.right) {
            for (
              let row = startRow;
              prevSelectionDirection.up ? row >= prevRow : row <= prevRow;
              prevSelectionDirection.up ? row-- : row++
            ) {
              for (
                let col = prevCol;
                deselected.left ? col > currentCol : col < currentCol;
                deselected.left ? col-- : col++
              ) {
                nextGrid[row][col] = isStartCellActive
              }
            }
          }
        } else {
          // default selection behavior
          for (
            let row = startRow;
            currentSelectionDirection.down
              ? row <= currentRow
              : row >= currentRow;
            currentSelectionDirection.down ? row++ : row--
          ) {
            for (
              let col = startCol;
              currentSelectionDirection.right
                ? col <= currentCol
                : col >= currentCol;
              currentSelectionDirection.right ? col++ : col--
            ) {
              nextGrid[row][col] = !isStartCellActive
            }
          }
        }
        if (isSelectionJustStarted) {
          nextGrid[startRow][startCol] = !isStartCellActive
        }

        return nextGrid
      }

      setSelectionPrevCoords({ prevRow: currentRow, prevCol: currentCol })
      setGrid(computeNextGrid())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    isSelectionInProgress,
    isStartCellActive,
    currentCol,
    currentRow,
    prevCol,
    prevRow,
    startCol,
    startRow,
  ])

  const startSelection = (row, col) => {
    setIsSelectionInProgress(true)
    setSelectionStartCoords({ startRow: row, startCol: col })
    setSelectionCurrentCoords({ currentRow: row, currentCol: col })
  }

  const stopSelection = () => {
    setIsSelectionInProgress(false)
    setSelectionStartCoords({})
    setSelectionCurrentCoords({})
    setSelectionPrevCoords({})
  }

  const handleCellHover = (row, col) => {
    if (isSelectionInProgress) {
      setSelectionCurrentCoords({ currentRow: row, currentCol: col })
    }
  }

  return (
    <div className="grid" onMouseLeave={stopSelection}>
      {grid.map((row, rowIdx) =>
        row.map((isCellActive, colIdx) => (
          <Cell
            key={`${rowIdx}:${colIdx}`}
            active={isCellActive}
            onMouseDown={() => startSelection(rowIdx, colIdx)}
            onMouseUp={stopSelection}
            onMouseEnter={() => handleCellHover(rowIdx, colIdx)}
          />
        ))
      )}
    </div>
  )
}

export default Grid
