export const generateGrid = (width, height) =>
  Array.from(Array(height), () => Array(width).fill(false))
